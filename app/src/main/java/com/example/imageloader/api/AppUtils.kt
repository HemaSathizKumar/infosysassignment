package com.example.imageloader.api

import com.example.imageloader.BuildConfig

class AppUtils {
    companion object {
        fun getSOService(): SOService {
            return RetrofitClient.getClient(BuildConfig.SERVER_URL)!!.create(SOService::class.java)
        }
    }

}
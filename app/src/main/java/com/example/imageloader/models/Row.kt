package com.example.imageloader.models

data class Row(
    val description: String = "",
    val imageHref: String = "",
    val title: String = ""
)
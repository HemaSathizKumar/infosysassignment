package com.example.imageloader.ui.main

import android.app.Activity
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.widget.Toast
import com.example.imageloader.R
import com.example.imageloader.api.AppUtils
import com.example.imageloader.helper.ConstantHelper
import com.example.imageloader.helper.NetworkHelper
import com.example.imageloader.models.Blog
import com.example.imageloader.models.Row
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException


class BlogViewModel : ViewModel() {
    private var blogData = MutableLiveData<Blog>()
    var blogArrayList: ObservableList<Row> = ObservableArrayList()
    private var errorDetails = MutableLiveData<String>()


    fun fetchBlogDetails(activity: Activity) {
        val repository = AppUtils.getSOService()
        if (NetworkHelper.haveNetworkConnection(activity)) {
            CompositeDisposable().add(
                repository.getBlogDetails()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                        { result ->
                            blogData.postValue(result)
                        },
                        { error ->
                            loadError(error, activity)
                        })
            )
        }else{
            errorDetails.postValue(ConstantHelper.ERROR)

        }
    }

    fun loadError(error: Throwable, activity: Activity) {
        when (error) {
            is HttpException -> {
                val responseBody = error.response().errorBody()
                responseBody?.let {
                    errorDetails.postValue(ConstantHelper.ERROR)
                }
            }
            is SocketTimeoutException -> {
                errorDetails.postValue(ConstantHelper.ERROR)
            }
            is IOException -> {
                errorDetails.postValue(ConstantHelper.ERROR)
            }
            else -> {
                error.message?.let {
                    errorDetails.postValue(ConstantHelper.ERROR)
                }
            }
        }
    }

    fun getBlogDetails(): MutableLiveData<Blog> {
        return blogData
    }

    fun getErrorResponseBlogDetails(): MutableLiveData<String> {
        return errorDetails
    }

    fun addBlogItemsToList(rows: List<Row>) {
        blogArrayList.clear()
        blogArrayList.addAll(rows)
    }
}

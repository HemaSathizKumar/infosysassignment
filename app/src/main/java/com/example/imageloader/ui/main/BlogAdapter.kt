package com.example.imageloader.ui.main

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.imageloader.R
import com.example.imageloader.databinding.ItemBlogBinding
import com.example.imageloader.models.Row

class BlogAdapter : RecyclerView.Adapter<BlogAdapter.ViewHolder>() {

    private val userGeographyDetailsList: ArrayList<Row> = ArrayList()

    override fun getItemCount() = userGeographyDetailsList.size

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): BlogAdapter.ViewHolder {
        val binding: ItemBlogBinding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_blog, parent, false
        ) as ItemBlogBinding
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        holder.bind(userGeographyDetailsList.get(p1))
    }

    fun addItems(arrayListUserGeographyDetails: List<Row>) {
        userGeographyDetailsList.addAll(arrayListUserGeographyDetails)
        notifyDataSetChanged()
    }

    fun clearItems() {
        userGeographyDetailsList.clear()
    }

    inner class ViewHolder(var itemListBinding: ItemBlogBinding) : RecyclerView.ViewHolder(itemListBinding.root) {

        private lateinit var mBlogItemViewModel: ItemBlogDetailsListViewModel

        fun bind(row: Row) {
            mBlogItemViewModel = ItemBlogDetailsListViewModel(row)
            itemListBinding.viewModel = mBlogItemViewModel
            itemListBinding.executePendingBindings()
        }
    }
}
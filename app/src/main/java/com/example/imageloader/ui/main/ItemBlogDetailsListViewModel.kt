package com.example.imageloader.ui.main

import android.databinding.BaseObservable
import android.databinding.ObservableField
import com.example.imageloader.models.Row

class ItemBlogDetailsListViewModel(row: Row) : BaseObservable() {

    var title: ObservableField<String> = ObservableField()
    var description: ObservableField<String> = ObservableField()
    var imageUrl: ObservableField<String> = ObservableField()

    init {
        title.set(row.title)
        description.set(row.description)
        imageUrl.set(row.imageHref)
    }
}
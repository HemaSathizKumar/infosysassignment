package com.example.imageloader

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.imageloader.ui.main.BlogFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.clRootView, BlogFragment.newInstance())
                .commitNow()
        }
    }

}

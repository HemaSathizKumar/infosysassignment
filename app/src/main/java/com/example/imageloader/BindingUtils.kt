package com.example.imageloader

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.imageloader.models.Row
import com.example.imageloader.ui.main.BlogAdapter


object BindingUtils {

    @JvmStatic
    @BindingAdapter("blogadapter")
    fun addBlogItems(recyclerView: RecyclerView, listPhotos: List<Row>?) {
        val adapter = recyclerView.adapter as BlogAdapter
        if (adapter != null && listPhotos != null && !listPhotos.isEmpty()) {
            adapter.clearItems()
            adapter.addItems(listPhotos)
        }
    }

    @JvmStatic
    @BindingAdapter("app:imageResource")
    fun setImageUrl(imageView: ImageView, url: String?) {
        val context = imageView.context
        if (url != null) {
            Glide.with(context).load(url).placeholder(context.resources.getDrawable(R.drawable.ic_launcher_background))
                .into(imageView)
        }
    }
}